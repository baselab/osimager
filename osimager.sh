#!/bin/bash
#
# Project page: https://bitbucket.org/baselab/osimager
# CopyFree, BaseLab <baselab@hushmail.com>
# This script is available under a CopyFree license.
# View LICENSE file for more info.
#


### variables
LABEL="OSImager"
VERSION="0.1"
DEV_URL="https://bitbucket.org/baselab/osimager"
DEV_WHO="BaseLab <baselab@hushmail.com>"

BASH="$(which bash)"
PROGRAM="${0}"
PROGNAME="${PROGRAM##*/}"
ARG1="${1}"

WHOAMI="$(whoami)"

LOGDIR="/var/tmp"
#LOGFILE="${PROGNAME}_$(date +%Y%m%d-%H%M%S).log"
LOGFILE="${PROGNAME}.log"
LOGDEST="${LOGDIR}/${LOGFILE}"

eval declare -a UTIL_EXEC=(
    "lsblk"
    "mkfs"
    "cfdisk"
    "fsck"
    "mkswap"
    "sfdisk")

eval declare -a CORE_EXEC=(
    "dd"
    "mkdir"
    "df"
    "du"
    "mkdir"
    "sync"
    "chroot")

eval declare -a PART_EXEC=(
    "mount"
    "parted"
    "partimage"
    "fsarchiver"
    "mke2fs"
    "mkdosfs"
    "mkntfs")

eval declare -a ARCH_EXEC=(
    "tar"
    "gzip"
    "bzip2")

eval declare -a NETW_EXEC=(
    "ip"
    "ftp-ssl"
    "ssh"
    "scp"
    "mount.nfs")

eval declare -a MISC_EXEC=(
    "grep"
    "mawk"
    "sed")


###
usage() {
    if [[ -n "$ARG1" ]] ; then
    cat <<EOF
${LABEL} ${VERSION}

    Syntax: ${BASH} ${PROGRAM}

Project page: ${DEV_URL}
CopyFree, ${DEV_WHO}
EOF
    exit 1
    fi
}

do_log() {
    if [[ -w "${LOGDEST}" ]] ; then
        tee -a "${LOGDEST}"
    fi
}

log() {
    local message="${2}"
    if [[ "$1" = "-i" ]] ; then
        local type="info"
    elif [[ "$1" = "-g" ]] ; then
        local type="good"
    elif [[ "$1" = "-w" ]] ; then
        local type="warn"
    elif [[ "$1" = "-f" ]] ; then
        local type="fail"
        if [[ -z "${2}" ]] ; then
            local message="Unrecoverable error."
        fi
    fi
    echo -e "$(date '+%b %d %T') - ${PROGRAM}: <${type}> ${message}"
}

ck_log() {
    touch "${LOGDEST}" 2>/dev/null
    if [[ -w "${LOGDEST}" ]] ; then
        log -g "Log writable."
    else
        log -w "Problem with '${LOGDIR}' directory. Log disabled."
    fi
}

ck_root() {
    if [[ "${WHOAMI}" = "root" ]] ; then
        log -g "Root privileges OK."
    else
        log -f "This script requires root privileges to operate."
        exit 1
    fi
}

ck_exec() {
    eval declare -a plist=${1#*=}
    for e in ${plist[@]} ; do
        if [[ -x "$(which ${e})" ]] ; then
            log -g "Found: $(which ${e})"
        else
            log -f "'${e}': Not found or not executable. Exiting."
            exit 1
        fi
    done
}

ck_devices() {
    declare -A Adesc
    declare -a Adev
    local list="$( lshw -short -quiet -c disk |
        awk 'NR <= 2 {next} {$1=$3=""} 1' |
        sed 's/^ //g ; s/  /;/g ; s/ /_/g ; s/;/ /g ' |
        tr "\n" " " )"
    local dev_n="$( lsblk | grep -c disk )"
    local dev_t="$((dev_n*2))"

    for i in $(seq 1 2 ${dev_t}) ; do
        e="$((i+1))"
        dev="$(echo ${list} | awk -v i=$i '{print $i}')"
        desc="$(echo ${list} | awk -v e=$e '{print $e}')"
        Adesc[${dev}]="${desc}"
    done
    local Adev=(${!Adesc[@]})
    local keys="$((dev_n-1))"

    for i in $(seq 0 1 ${keys}) ; do
        echo -e "\nDevice ${Adev[$i]}: ${Adesc[${Adev[$i]}]}"
        lsblk -i -o NAME,SIZE,TYPE,FSTYPE,MOUNTPOINT,LABEL ${Adev[$i]}
    done
}

ck_removable() {
    echo
}

ck_network() {
    echo
}

main() {
    log -i "${LABEL} ${VERSION}"
    log -i "${DEV_URL}"
    log -i "${DEV_WHO}"
    log -i "Starting..."
    ck_root
    log -i "Checking required programs:"
    ck_exec "$(declare -p UTIL_EXEC)"
    ck_exec "$(declare -p CORE_EXEC)"
    ck_exec "$(declare -p PART_EXEC)"
    ck_exec "$(declare -p ARCH_EXEC)"
    ck_exec "$(declare -p NETW_EXEC)"
    ck_exec "$(declare -p MISC_EXEC)"
    ck_devices
}

usage
ck_log

main 2>&1 | do_log

