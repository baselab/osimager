# OSImager

## What OSImager is
+ A bash script to imaging systems/disks/partitions
+ A bash script to restore such saved images

## What OSImager is not
+ A backup software
+ A HDD rescue program

## Prerequisites
### Privileges
+ Full root permissions

### Software and executables
#####Programs needed:
+ bash
+ util-linux
+ coreutils
+ mount
+ grep
+ iproute2
+ parted
+ partimage
+ fsarchiver
+ tar
+ gzip
+ bzip2
+ e2fsprogs
+ dosfstools
+ ntfs-3g
+ nfs-common
+ ftp (with ssl)
+ openssh (client)

### Hardware
+ A USB/internal extra drive to save/restore the images
+ or, a remote NFS/FTP/SSH network share

## Usage
### Intro
#####To imaging a system with OSImager we'll have two steps:
+ Gathering infos about what is being saved/restored (interacive)
+ Actually save/restore the image(s) (automagic)

### Real Usage
1. Boot the machine from a linux system wherefrom the script will be
launched
2. Run the script

    `# bash /path/to/osimager.sh`

3. Follow the instructions and then wait.
